#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <vector>
#include <queue>

class MyNetBpmGraph {
public:
	explicit MyNetBpmGraph(const std::string& filename);
	void shortestPathToPbm(const std::string& filename, uint16_t threshold);

private:
	// Methods
	std::vector<int64_t> shortestPath(uint16_t threshold);
	std::vector<uint64_t> getNeighbors(uint64_t i, uint16_t threshold);
	std::pair<uint64_t, uint64_t> getCoordinates(std::pair<uint64_t, uint16_t> item);

	// Variables
	uint64_t rows_ = 0;
	uint64_t columns_ = 0;
	uint16_t maxValue_ = 0;
	std::vector<std::pair<uint64_t, uint16_t>> matrix_;
};

MyNetBpmGraph::MyNetBpmGraph(const std::string& filename) {
	const std::string VALID_VERSION = "P2";
	const char COMMENT = '#';

	const uint8_t VERSION_LINE = 0;
	const uint8_t DIMENSIONS_LINE = 1;
	const uint8_t MAXVALUE_LINE = 2;

	std::ifstream inputFile(filename);
	if (!inputFile) {
		std::cerr << "Unable to open input file! Committing suicide..." << std::endl;
		std::exit(EXIT_FAILURE);
	}

	std::string line;
	uint8_t lineCount = 0;
	uint16_t column = 0, row = 0;
	uint64_t index = 0;

	// Read input
	while (std::getline(inputFile, line)) {
		// Skip comments
		if (line[0] == COMMENT) {
			continue;
		}

		std::istringstream ss(line);

		if (lineCount == VERSION_LINE) {
			if (line != VALID_VERSION) {
				throw std::invalid_argument("Invalid version found in file header!");
			}
			lineCount++;
		} else if (lineCount == DIMENSIONS_LINE) {
			ss >> columns_ >> rows_;
			matrix_.reserve(columns_ * rows_);
			lineCount++;
		} else if (lineCount == MAXVALUE_LINE) {
			ss >> maxValue_;
			lineCount++;
		} else {
			uint16_t number = 0;
			while ((ss >> number)) {
				if (column == columns_) {
					column = 0;
					row++;
				}
				if (row == rows_) {
					row = 0;
				}
				matrix_.emplace_back(index++, number);
				column++;
			}
		}
	}

	inputFile.close();
}

std::pair<uint64_t, uint64_t> MyNetBpmGraph::getCoordinates(std::pair<uint64_t, uint16_t> item) {
	return std::make_pair(item.first / rows_, item.first % rows_);
}

std::vector<int64_t> MyNetBpmGraph::shortestPath(uint16_t threshold) {
	auto result =  std::vector<int64_t>();
	auto parent = std::vector<int64_t>(rows_ * columns_, -1);
	std::vector<uint64_t > distance(rows_ * columns_, std::numeric_limits<uint64_t >::max());
	std::priority_queue< std::pair<uint64_t, uint64_t>, std::vector <std::pair<uint64_t, uint64_t>> , std::greater<> > minHeap;

	minHeap.push(std::make_pair(0, 0));
	distance[0] = 0;

	while (!minHeap.empty()) {
		uint64_t i = minHeap.top().second;
		minHeap.pop();

		auto neighbors = getNeighbors(i, threshold);

		for (auto item : neighbors) {
			uint64_t v = matrix_[item].first;

			if (distance[v] > distance[i] + 1) {
				distance[v] = distance[i] + 1;
				parent[v] = i;
				minHeap.emplace(distance[v], v);
			}

			if (item == columns_ * rows_ - 1) {
				// Found a solution!
				while (!minHeap.empty()) {
					minHeap.pop();
				}
			}
		}
	}

	int i = parent[columns_ * rows_ - 1];

	while (i != -1) {
		result.push_back(i);
		i = parent[i];
	}

	return result;
}

std::vector<uint64_t> MyNetBpmGraph::getNeighbors(uint64_t i, uint16_t threshold) {
	auto result =  std::vector<uint64_t>();

	// Za tuto metodu sa uprimne ospravedlnujem, potreboval som rychle implementacne riesenia (treba pisat bakalarku)
	// Dovolil som si tak aplikovat postup: if it works, it's not stupid
	// Pointa je, nechcelo sa mi pocitat, vzdy sa pozriem a ked to spadne tak to spadne

	try {
		if (threshold <= abs(matrix_.at(i).second - matrix_.at(i - columns_ - 1).second)) {
			result.push_back(i - columns_ - 1);
		}
	} catch (const std::out_of_range &e) {}

	try {
		if (threshold > abs(matrix_.at(i).second - matrix_.at(i - columns_).second)) {
			result.push_back(i - columns_);
		}
	} catch (const std::out_of_range &e) {}

	try {
		if (threshold > abs(matrix_.at(i).second - matrix_.at(i - columns_ + 1).second)) {
			result.push_back(i - columns_ + 1);
		}
	} catch (const std::out_of_range &e) {}

	try {
		if (threshold > abs(matrix_.at(i).second - matrix_.at(i + 1).second) && getCoordinates(matrix_.at(i)).second != columns_ - 1) {
			result.push_back(i + 1);
		}
	} catch (const std::out_of_range &e) {}

	try {
		if (threshold > abs(matrix_.at(i).second - matrix_.at(i + columns_ - 1).second) && getCoordinates(matrix_.at(i)).second != 0) {
			result.push_back(i + columns_ - 1);
		}
	} catch (const std::out_of_range &e) {}

	try {
		if (threshold > abs(matrix_.at(i).second - matrix_.at(i + columns_).second)) {
			result.push_back(i + columns_);
		}
	} catch (const std::out_of_range &e) {}

	try {
		if (threshold > abs(matrix_.at(i).second - matrix_.at(i + columns_ + 1).second)) {
			result.push_back(i + columns_ + 1);
		}
	} catch (const std::out_of_range &e) {}

	return result;
}

void MyNetBpmGraph::shortestPathToPbm(const std::string& filename, uint16_t threshold) {
	auto path = shortestPath(threshold);
	auto result = std::vector<uint64_t>(rows_ * columns_, 0);
	const u_short MAX_COLUMN_SIZE = 70;

	for (auto item : path) {
		result[item] = 1;
	}

	// Chyba nam lavy spodny roh
	result[columns_ * rows_ - 1] = 1;

	std::ofstream outputFile(filename);
	if (!outputFile) {
		std::cerr << "Unable to open output file! Committing suicide..." << std::endl;
		std::exit(EXIT_FAILURE);
	}

	outputFile << "P1" << std::endl;
	outputFile << columns_ << " " << rows_ << std::endl;

	uint64_t column = 0;
	for (auto item : result) {
		if (column == MAX_COLUMN_SIZE) {
			column = 0;
			outputFile << std::endl;
		}

		outputFile << std::to_string(item) << " ";
		column++;
	}

	outputFile.close();
}

int main(int argc, char** argv) {
	const uint8_t INPUT_FILE_ARGUMENT = 1;
	const uint8_t OUTPUT_FILE_ARGUMENT = 2;
	const uint8_t THRESHOLD_ARGUMENT = 3;

	if (argc != 4) {
		std::cerr << "Invalid number of arguments! Committing suicide..." << std::endl;
		std::exit(EXIT_FAILURE);
	}

	auto myGraph = MyNetBpmGraph(argv[INPUT_FILE_ARGUMENT]);

	myGraph.shortestPathToPbm(argv[OUTPUT_FILE_ARGUMENT], std::stoi(argv[THRESHOLD_ARGUMENT]));
}