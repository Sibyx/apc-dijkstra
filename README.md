# Dijkstra PNM

Vytvorte konzolovú aplikáciu s troma parametrami:

- Vstupný súbor
- Výstupný súbor
- Prah (celé číslo 16bit, teda 0-65535 rozsah)

Vstupný súbor bude vo formáte [NetBpm](http://netpbm.sourceforge.net/doc/pnm.html) (prípadne iný popis). Je to súborný 
formát šiestich typov. Rozlišujú sa podľa prvých dvoch bajtov (od P1 až po P6), kde prvé tri sú textové a ďalšie 
binárne. Nás bude zaujímať iba formát P2, ktorý je čiernobiely. V podstate na začiatku máte tento magic P2, potom je 
veľkosť horizontálna a vertikálne, za nimi nasleduje maximálna hodnota v rastri a potom samotný raster. V popise formátu 
je aj definovaná dĺžka riadkov (max 70 znakov), toto ale pri vstupe riešiť nebudeme, programy sú v tomto veľmi 
liberálne. Navyše komentáre môžu byť technicky aj mimo začiatku riadku, také znovu riešiť nebudeme. Komentáre môžu byť 
iba na začiatku riadku, inak môžete dať chybu. Ak čokoľvek vo formáte nesedí skončite s chybou.

Otvoriť takýto súbor by mal zvládnuť každý dobrý prehliadať obrázkov (napr. [Faststone](https://www.faststone.org/), 
alebo [IrfanView](https://www.irfanview.com/), oba sú aj ako portable aplikácie). Rovnako sa takýto formát dá aj ľahko 
vytvoriť.

Keď už bude súbor načítaný, tak použijeme 
[algoritmus na nájdenie najkratšej cestičky](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) medzi ľavým horným 
a pravým dolným rohom. Každý jeden pixel predstavuje bod grafu, a je spojený hranou so susednými ak rozdiel ich hodnôt 
jasu (hodnoty zadané v rastri) je menší, alebo rovný ako zadaný prah (tretí parameter aplikácie). Pričom uvažujeme 8 
susednosť (čiže aj diagonálne).

Príklad pre prah `40`, zelená čiara znamená spojené vrcholy. 

![](assets/example.png)

Ak cestička neexistuje, tak program skončí s chybou, ak existuje, tak vytvorí výstupný súbor vo formáte P1. kde bude 
cestička (tie pixly cez ktoré ide) vyznačená čiernou. Na výstupe dodržte aj limit 70 znakov na riadok. 


## Zdroje

- [Netpbm format](https://en.wikipedia.org/wiki/Netpbm_format)